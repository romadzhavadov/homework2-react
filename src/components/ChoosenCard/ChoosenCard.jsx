import React from "react";
import styles from "./ChoosenCard.module.scss";
import PropTypes from "prop-types";

const ChoosenCard = (props) => {
    const { url, name, price, color, count} = props;

    return(
        <section>

        <div className={styles.wrapper}>
            <img className={styles.img} src={url} alt={name} />
            <p className={styles.caption}>{name}</p>
            <div className={styles.infoWrap}>
                <span className={styles.info}>your price: {price} $</span>
                <span className={styles.desc}> color: {color}</span>
            </div>
            <p className={styles.info}>Number: {count}</p>
        </div>

        </section>
    )

}

ChoosenCard.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    color: PropTypes.string,
    count: PropTypes.number,
}

ChoosenCard.defaultProps = {
    name: 'text',
    price: 'text',
    url: 'text',
    count: 0,
    color: 'text',
}


export default ChoosenCard;