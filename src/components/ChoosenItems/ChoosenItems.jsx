import React from "react";
import ChoosenCard from "../ChoosenCard/ChoosenCard";
import PropTypes from "prop-types";



const ChoosenItems = (props) => {

    const { items } = props;

    return(
        <section>
            <div className="cardWrap">
            <h2>Choosen Laptops</h2>

                {items && items.map(({name, price, url, id, color, isFavourite, count}) => <ChoosenCard key={id} name={name} price={price} url={url} id={id} color={color} count={count} isfavourite={isFavourite}/> )}
            </div>
        </section>
    )

}

ChoosenItems.propTypes = {
    items: PropTypes.array,
}

ChoosenItems.defaultProps = {
    items: [],
}


export default ChoosenItems;